//Create a modal for employees table
module.exports = function (sequelize, Sequelize) {
    var Salaries = sequelize.define('salaries', {
        emp_no: {
            type:Sequelize.INTEGER,
            primaryKey: true
        },
        salary: Sequelize.INTEGER,
        from_date: Sequelize.DATE,
        to_date: Sequelize.DATE

    }, {
        timestamps: false
    });
    return Salaries;
}

