(function () {
    angular
        .module("EmployeeApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http", "EmployeeServices"];

    function SearchCtrl($http, EmployeeServices) {
        var vm = this;


        vm.empName = '';
        vm.result = null;
        vm.showSalary = false;
        vm.names = [
            {
                displayName: "First Name",
                dbName: 'first_name'
            },
            {
                displayName: "Last Name",
                dbName: 'last_name'
            }
        ];

        vm.selectedName = vm.names[0];

        vm.search = function () {
            console.log('--------------vm.empName-----------', vm.empName);
            vm.showSalary = false;
            EmployeeServices.getEmployee(vm.empName,vm.selectedName.dbName)
                .then(function (response) {
                    console.info("success");
                    vm.result = response
                }).catch(function (response) {
                console.info("Error");
                vm.status.message = response.message;
                vm.status.code = 400;
            });
        };

        vm.searchForSalary = function () {
            console.log('--------------vm.empName-----------', vm.empName);
            vm.showSalary = true;
            EmployeeServices.getEmployeeSalary(vm.empName,vm.selectedName.dbName)
                .then(function (response) {
                    console.info("success");
                    vm.result = response;
                }).catch(function (response) {
                console.info("Error");
                vm.status.message = response.message;
                vm.status.code = 400;
            });
        };
    };
})();






