(function () {
    angular
        .module("EmployeeApp")
        .service("EmployeeServices", EmployeeServices);

    EmployeeServices.$inject = ["$http", "$q"];

    function EmployeeServices($http, $q) {
        var obj = {};
        obj.setEmployee = function (employee) {
            var deffered = $q.defer();
            $http.post("/api/employee", employee)
                .then(function () {
                    console.info("success");
                    deffered.resolve({message: "The employee is added to the database."});

                }).catch(function () {
                console.info("error");
                deffered.reject({message: "Failed to add the employee to the database."});
            });
            return deffered.promise;
        };

        obj.getEmployee = function (employeeName,employeeDBName) {
            var deffered = $q.defer();
            $http.get("/api/employee" ,{
                params: { 'emp_name': employeeName ,'db_name' :employeeDBName }
                } )
                .then(function (result) {
                    console.info("success");
                    deffered.resolve(result.data);

                }).catch(function () {
                console.info("error");
                deffered.reject({message: "Failed to get the employee from the database."});
            });
            return deffered.promise;
        };

        obj.getEmployeeSalary = function (employeeName,employeeDBName) {
            var deffered = $q.defer();
            $http.get("/api/employeeSalary/" ,{
                    params: { 'emp_name': employeeName ,'db_name' :employeeDBName}
                })
                .then(function (result) {
                    console.info("success");
                    deffered.resolve(result.data);

                }).catch(function () {
                console.info("error");
                deffered.reject({message: "Failed to get the employee salary from the database."});
            });
            return deffered.promise;
        };
        return obj;
    };
})();






