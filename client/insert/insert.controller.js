(function() {
    angular
        .module("EmployeeApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http","EmployeeServices"];

    function InsertCtrl($http,EmployeeServices) {
        var vm = this;
        vm.employee = {};
        vm.empNo="";
        vm.employee.firstname = "";
        vm.employee.lastname = "";
        vm.employee.gender = "";
        vm.employee.birthday = "";
        vm.employee.hiredate = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.register = function () {
            EmployeeServices.setEmployee(vm.employee)
                .then(function (response) {
                    console.info("success");
                    vm.status.message = response.message;
                    vm.status.code = 202;
                }).catch(function (response) {
                console.info("Error");
                vm.status.message = response.message;
                vm.status.code = 400;
            });
        };
    };
})();






